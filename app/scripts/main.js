var app = {

  init: function () {
    app.windowResize();
    app.fullScreenHeight();
    app.custom();
    app.menu();
    app.modals();
    app.selectric();
    app.tabs();
    app.sliders();
    app.validate();
    app.accordeon();

  },

  windowResize: function () {
    $(window).on('resize', function () { });
  },

  windowLoad: function () {
    $(window).on('load', function () { });
  },

  fullScreenHeight: function () {
    $(window).on('resize load', function () {
      var headHeight = 0,
        footHeight = 0;

      if ($('header').length) {
        headHeight = $('header').outerHeight();
      }
      if ($('footer').length) {
        footHeight = $('footer').outerHeight();
      }

      const height = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
      $('main').css('min-height', height - headHeight - footHeight);
      $('.jsCustomHeight').css('min-height', height);
    });
  },


  custom: function () {
    $('.b_header').sticky({
      topSpacing: 0,
      zIndex: 50
    });

    $.protip({
      selector: '.jsTooltip',
      defaults: {
        position: 'bottom-right-corner',
        skin: 'square',
        scheme: 'black',
        width: 270
      }
    });
    

    $('.btn_phone_toggle').on('click', function(){
      $(this).toggleClass('open');
      $('.phone_list').slideToggle(300);
    });
    $('.btn_phone').on('click', function(e){
      e.preventDefault();
      $(this).next('.phone_list').fadeToggle(300);
    });
    $('.close_list').on('click', function(e){
      e.preventDefault();
      $(this).parents('.phone_list').fadeToggle(300);
    });


    $(window).on('load', function () {
      if ($(window).width() < 768) {
        $('.b_customers .customers_wrapper img').each(function(){
          var width = $(this).width();
          width = width * 0.6;
          $(this).width(width);
        });
      }
    });

    $(window).on('load', function () {
      $('.jsCustomScrollbarX').each(function () {
        var $this = $(this);
        $this.mCustomScrollbar({
          scrollInertia: 300,
          axis: 'x'
        });
      });

      $('.jsCustomScrollbarY').each(function () {
        var $this = $(this);
        $this.mCustomScrollbar({
          scrollInertia: 300,
          axis: 'y'
        });
      });

      $('.jsCustomScrollbarYX').each(function () {
        var $this = $(this);
        $this.mCustomScrollbar({
          scrollInertia: 300,
          scrollbarPosition: 'outside',
          axis: 'yx'
        });
      });
    });



    $('.btn_filter').on('click', function () {
      var $this = $(this);
      var staticName = $(this).attr('data-static-name');
      var activeName = $(this).attr('data-active-name');
      $this.toggleClass('opened');
      if( $this.hasClass('opened') ){
        $this.text(activeName);
      }else{
        $this.text(staticName);
      }
      $('.filter_container').slideToggle(300);
    });

    $('.jsCloseFilter').on('click', function (e) {
      e.preventDefault();
      $('.btn_filter').removeClass('opened');
      $('.filter_container').slideUp(300);
    });



    $('#all_parameters').on('change', function () {
      if ($(this).is(':checked')) {
        $('.filter_custom').find('[type="checkbox"]').prop('checked', true);
      } else {
        $('.filter_custom').find('[type="checkbox"]').prop('checked', false);
      }
    });

    $('.jsCheckboxCategory').each(function () {
      var $this = $(this);
      $this.on('change', function () {
        if ($(this).is(':checked')) {
          $this.parents('.filter_custom_column').find('.filter_category_wrapper [type="checkbox"]').prop('checked', true);
        } else {
          $this.parents('.filter_custom_column').find('.filter_category_wrapper [type="checkbox"]').prop('checked', false);
          $('#all_parameters').prop('checked', false);
        }
      });
    });

    $('.filter_category_wrapper').each(function () {
      var $this = $(this);
      var $thischeckbox = $this.find('[type="checkbox"]');
      $thischeckbox.on('change', function () {
        $this.parents('.filter_custom_column').find('.jsCheckboxCategory').prop('checked', false);
        $('#all_parameters').prop('checked', false);
      });
    });



    $('.jsSaveTableFilter').on('click', function (e) {
      e.preventDefault();
      $(this).parents('.b_modal').magnificPopup('close');

      $('[type="checkbox"].table_column').each(function () {
        var table_column = $(this).attr('data-filter');

        if ($(this).is(':checked')) {
          $('.b_table_result').find('[data-column="' + table_column + '"]').show();
          $('.table_over.jsCustomScrollbarYX').mCustomScrollbar('destroy').mCustomScrollbar({
            scrollInertia: 100,
            scrollbarPosition: 'outside',
            axis: 'yx',
            callbacks:{
              whileScrolling: function(){
                var scrollHead = $(document).find('.dataTables_scroll .dataTables_scrollHead table.dataTable');
                scrollHead.css('margin-left', this.mcs.left+'px');
              }
            }
          });
        } else {
          $('.b_table_result').find('[data-column="' + table_column + '"]').hide();
          $('.table_over.jsCustomScrollbarYX').mCustomScrollbar('destroy').mCustomScrollbar({
            scrollInertia: 100,
            scrollbarPosition: 'outside',
            axis: 'yx',
            callbacks:{
              whileScrolling: function(){
                var scrollHead = $(document).find('.dataTables_scroll .dataTables_scrollHead table.dataTable');
                scrollHead.css('margin-left', this.mcs.left+'px');
              }
            }
          });
        }
      });
    });



    

    $(window).on('load', function () {
      $('#table').DataTable({
        // "scrollY": 700,
        'scrollX': true
      });

      $(document).on('change', '#table .checkbox [type="checkbox"]', function(){
        var $this = $(this);
        if( $this.is(':checked') ){
          $this.parents('tr').addClass('checked');
        }else{
          $this.parents('tr').removeClass('checked');
        }
      });

      
      
      $('.dataTables_scrollBody').mCustomScrollbar({
        scrollInertia: 100,
        scrollbarPosition: 'outside',
        axis: 'yx',
        callbacks:{
          whileScrolling: function(){
            var scrollHead = $(document).find('.dataTables_scroll .dataTables_scrollHead table.dataTable');
            scrollHead.css('margin-left', this.mcs.left+'px');
          }
        }
      });
    });
  },

  menu: function () {
    var $btnMenu = $('.jsMenu');
    $btnMenu.click(function () {
      $(this).toggleClass('menu-is-active');
      $('.header_menu').fadeToggle(300);
      $('body').toggleClass('menu_open');
    });
  },


  modals: function () {
    $('.jsOpenModals').magnificPopup({
      removalDelay: 300,
      mainClass: 'my-mfp-slide-bottom'
    });

    $('.jsCloseModal').on('click', function () {
      $(this).parents('.b_modal').magnificPopup('close');
    });
  },


  selectric: function () {
    $('.jsSelectricView').selectric({
      maxHeight: 150,
      disableOnMobile: false,
      nativeOnMobile: false
    });
  },


  tabs: function () {
    var tabs = $('.jsTabs');
    tabs.each(function () {
      var tabs = $(this),
        tab = tabs.find('.jsTabsTab'),
        content = tabs.find('.jsTabsItem');
      tab.each(function (index, element) {
        $(this).attr('data-tab', index);
      });

      function showContent(i) {
        tab.removeClass('-active');
        content.removeClass('-active').removeClass('-fade');
        tab.eq(i).addClass('-active');
        content.eq(i).addClass('-active');
        setTimeout(function () {
          content.eq(i).addClass('-fade');
        }, 1);
      }
      tab.on('click', function (e) {
        e.preventDefault();
        showContent(parseInt($(this).attr('data-tab')));
      });
    });
  },


  sliders: function () {

    $('.jsAlsoChoose').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      arrows: false,
      autoplay: true,
      autoplaySpeed: 5000,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3,
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            arrows: true,
          }
        },
      ]
    });

    $(window).on('resize load', function () {
      if ($(window).width() < 992) {
        if (!$('.jsReviewsSlider').hasClass('slick-initialized')) {
          $('.jsReviewsSlider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
          });
        }
      } else {
        if ($('.jsReviewsSlider').hasClass('slick-initialized')) {
          $('.jsReviewsSlider').slick('unslick');
        }
      }

      if ($(window).width() < 768) {
        if (!$('.jsCertificates').hasClass('slick-initialized')) {
          $('.jsCertificates').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
          });
        }
      } else {
        if ($('.jsCertificates').hasClass('slick-initialized')) {
          $('.jsCertificates').slick('unslick');
        }
      }
    });
  },


  validate: function () {

    // $('input[type="tel"]').mask('+7 (999) 999-99-99');

  },


  accordeon: function () {
    $('.jsAccord').each(function () {
      var accord = $(this),
        accord_btn = accord.find('.jsAccordBtn'),
        accord_content = accord.find('.jsAccordContent'),
        accord_item = accord.find('.jsAccordItem');

      accord_btn.on('click', function (e) {
        e.preventDefault();
        var $this = $(this),
          $this_item = $this.closest('.jsAccordItem'),
          $this_content = $this.closest('.jsAccordItem').find('.jsAccordContent');
        if ($this.hasClass('-active')) {
          $this.removeClass('-active');
          $this_content.slideUp();
          $this_item.removeClass('item_active');
        } else {
          accord_item.removeClass('item_active');
          accord_btn.removeClass('-active');
          accord_content.slideUp();
          $this.addClass('-active');
          $this_content.slideDown();
          $this_item.addClass('item_active');
        }
      });
    });
  }

};

$(document).ready(app.init());

app.windowLoad();
